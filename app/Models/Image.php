<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	use HasFactory;

	# protected $table = 'images';

	# Relación Uno a Muchos (One to Many)
	public function comments(){
		return $this->hasMany(Comment::class);
	}

	# Relación Uno a Muchos (One to Many)
	public function likes(){
		return $this->hasMany(Like::class);
	}

	# Relación de Muchos a Uno (Many to One) or (One to Many inverse)
	public function user(){
		return $this->belongsTo(User::class);
	}

}
