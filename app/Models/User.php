<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    # Relación Uno a Muchos (One to Many)
	public function images(){
		return $this->hasMany(Image::class);
	}
}
