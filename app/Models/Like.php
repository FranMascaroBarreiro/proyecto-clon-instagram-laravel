<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	use HasFactory;

	# protected $table = 'likes';

	# Relación de Muchos a Uno (Many to One) or (One to Many inverse)
	public function user(){
		return $this->belongsTo(User::class);
	}

	# Relación de Muchos a Uno (Many to One) or (One to Many inverse)
	public function images(){
		return $this->belongsTo(Image::class);
	}
}
