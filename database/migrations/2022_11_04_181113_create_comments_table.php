<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger('image_id');
			$table->text('content');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade'); /* UNSIGNED BIGINT */
			$table->foreign('image_id')->references('id')->on('images')->onUpdate('cascade')->onDelete('cascade'); /* UNSIGNED BIGINT */
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('comments');
	}
};
