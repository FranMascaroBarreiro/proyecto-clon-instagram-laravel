<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('role', 20);
			$table->string('name', 100);
			$table->string('surname', 200);
			$table->string('nick', 100);
			$table->string('email', 255);
			$table->string('password', 255);
			$table->string('image', 255);
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->string('rememebr_token', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
};
