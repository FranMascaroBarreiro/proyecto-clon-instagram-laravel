<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Fac tories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
	/**
	 * Define the model's default state.
	 *
	 * @return array<string, mixed>
	 */
	public function definition()
	{
		return [
			'role' => new Sequence (
				['role' => 'user'],
				['role' => 'admin']
			), /* 'user' */
			'name' => fake()->firstName(), /* 'Fran' */
			'surname' => fake()->lastName(), /* 'Mascaró' */
			'nick' => Str::random(7), /* 'fmb.own' */
			'email' => fake()->unique()->safeEmail(), /* 'algo@otro.com' */
			'pasword' => fake()->password(), /* 'abc123..' */
			'image' => NULL, /* null */
			'created_at' => fake()->dateTime(), /* curdate() */
			'updated_at' => fake()->dateTime(), /* curdate() */
			'remember_token' => Str::random(10) /* null */
		];
	}
}
