# PROYECTO CLON INSTAGRAM - LARAVEL

### INFO

- Para realizar este proyecto contaré con una serie de recursos adicionales creados por mí y alojados en la carpeta `/docs`

	· **database_schema.sql**: Contiene la estructura en SQL puro para las tablas de Base de Datos, posteriormente las transcribiré al lenguaje de Migraciones propia de Laravel para ceñirme lo máximo posible a las dinámicas de Laravel.

	· Vamos a nutrir la tabla de Usuarios mediante una Fábrica de Modelos. Podemso revisar la documentación a través de la web oficial de FakerPHP (https://fakerphp.github.io/formatters/)